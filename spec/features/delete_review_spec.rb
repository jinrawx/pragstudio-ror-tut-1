require 'rails_helper'

describe "Deleting a review" do
  it "destroys the review and shows the movie review listings without the deleted review" do
    movie = Movie.create(movie_attributes)

    review = movie.reviews.create(review_attributes)

    visit movie_reviews_path(movie)

    click_link 'Delete'

    expect(current_path).to eq(movie_reviews_path(movie))

    expect(page).not_to have_text(review.name)

    expect(page).not_to have_text(review.comment)

    expect(page).to have_text("Review successfully deleted!")
  end
end
